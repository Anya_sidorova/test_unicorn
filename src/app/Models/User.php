<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Carbon;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'second_name',
        'first_name',
        'middle_name',
        'email',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function logins(): HasMany
    {
        return $this->hasMany(LoginSource::class);
    }

    public function actions(): BelongsToMany
    {
        return $this->belongsToMany(Action::class, 'user_actions');
    }

    public static function scopeGroupA($query)
    {
        return $query->whereHas('logins');
    }

    public static function scopeGroupB($query)
    {
        return $query->whereHas('logins', static function ($query) {
            $query->select('users.id');
            $query->where('created_at', '>', Carbon::now()->subMonth())->groupBy('user_id')->havingRaw('COUNT(*) > 2');
        });
    }

    public static function scopeGroupC($query)
    {
        $name = 'акция1';
        $sale = Action::getActionByName($name);
        $start = Carbon::now()->subMonth()->startOfMonth()->toDateString();
        $end = Carbon::now()->subMonth()->endOfMonth()->toDateString();

        return $query->whereHas('logins', function ($query) use ($start, $end) {
            $query->whereBetween('tms', [Carbon::parse($start), Carbon::parse($end)])->groupBy('user_id');
        })->whereDoesntHave('logins', function ($query) use ($sale) {
            $query->whereBetween('tms', [Carbon::parse($sale->date_start), Carbon::parse($sale->date_end)]);
        });
    }
}
