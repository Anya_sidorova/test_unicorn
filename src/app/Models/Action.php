<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Action extends Model
{
    use HasFactory;

    protected $table = 'actions';

    public static function getActionByName(string $name)
    {
        return self::query()->where('title', '=', $name)->first();
    }

    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class);
    }
}
