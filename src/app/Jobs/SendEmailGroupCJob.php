<?php

namespace App\Jobs;

use App\Mail\SendMailGroupC;
use Illuminate\Support\Facades\Mail;

class SendEmailGroupCJob extends SendEmailJob
{
    public function handle(): void
    {
        Mail::to($this->user->email)->send(new SendMailGroupC($this->user));
    }
}
