<?php

namespace App\Jobs;

use App\Mail\SendMailGroupA;
use Illuminate\Support\Facades\Mail;

class SendEmailGroupAJob extends SendEmailJob
{
    public function handle(): void
    {
        Mail::to($this->user->email)->send(new SendMailGroupA($this->user));
    }
}
