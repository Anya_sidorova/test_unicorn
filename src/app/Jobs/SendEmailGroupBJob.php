<?php

namespace App\Jobs;

use App\Mail\SendMailGroupB;
use Illuminate\Support\Facades\Mail;

class SendEmailGroupBJob extends SendEmailJob
{
    public function handle(): void
    {
        Mail::to($this->user->email)->send(new SendMailGroupB($this->user));
    }
}
