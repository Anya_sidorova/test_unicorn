<?php

namespace App\Console\Commands;

use App\Jobs\SendEmailGroupBJob;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class SendMailGroupBCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:send-mail-b-group';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sends letters to users of group b';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $users = User::groupB()->get();
        foreach ($users as $user) {
            dispatch(new SendEmailGroupBJob($user));
        }
        return "Email sent successfully!";
    }
}
