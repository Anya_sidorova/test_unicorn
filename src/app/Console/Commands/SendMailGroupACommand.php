<?php

namespace App\Console\Commands;

use App\Jobs\SendEmailGroupAJob;
use App\Models\User;
use Illuminate\Console\Command;

class SendMailGroupACommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:send-mail-a-group';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sends letters to users of group a';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $users = User::groupA()->get();
        foreach ($users as $user) {
            dispatch(new SendEmailGroupAJob($user));
        }
        return "Email sent successfully!";
    }
}
