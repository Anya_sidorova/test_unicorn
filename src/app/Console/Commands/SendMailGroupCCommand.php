<?php

namespace App\Console\Commands;

use App\Jobs\SendEmailGroupCJob;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class SendMailGroupCCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:send-mail-c-group';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sends letters to users of group c';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $users = User::groupC()->get();
        foreach ($users as $user) {
            dispatch(new SendEmailGroupCJob($user));
        }
        return "Email sent successfully!";
    }
}
