<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Log;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        Log::debug(now());
//        $schedule->command('app:send-mail-a-group')->everyMinute();
        $schedule->command('app:send-mail-a-group')->dailyAt('10:00');
//
//        $schedule->command('app:send-mail-b-group')->everyMinute();
        $schedule->command('app:send-mail-b-group')->dailyAt('14:00');
//
//        $schedule->command('app:send-mail-c-group')->everyMinute();
        $schedule->command('app:send-mail-c-group')->dailyAt('18:00');

    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
