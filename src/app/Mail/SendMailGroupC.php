<?php

namespace App\Mail;

use App\Models\Action;
use Carbon\Carbon;

class SendMailGroupC extends SendMail
{
    public function build(): SendMail
    {
        $name = 'акция1';
        $sale = Action::getActionByName($name);
        $title = $sale->title;
        $date = Carbon::parse($sale->date_end);
        $dateEnd = $date->locale('ru')->isoFormat('D MMMM');


        return $this->from('anya_sidorova_2015@bk.ru', 'Магазин')
            ->view('emails.mail')
            ->subject('Новости и акции')
            ->with([
                'text' => "Здравствуйте, $this->secondName  $this->firstName  $this->middleName, Вы выбраны
                для участия в акции $title. Успейте до $dateEnd принять участие."
            ]);
    }
}


