<?php

namespace App\Mail;

class SendMailGroupA extends SendMail
{
    public function build(): SendMail
    {
        return $this->from('anya_sidorova_2015@bk.ru', 'Магазин')
            ->view('emails.mail')
            ->subject('Новости и акции')
            ->with([
                'text' => "Здравствуйте, $this->secondName  $this->firstName  $this->middleName, Вы давно не появлялись на сервисе, узнайте последние новости по ссылке: https://google.ru"
            ]);

    }
}
