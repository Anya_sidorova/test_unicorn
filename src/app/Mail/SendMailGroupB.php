<?php

namespace App\Mail;

class SendMailGroupB extends SendMail
{
    public function build(): SendMail
    {
        return $this->from('anya_sidorova_2015@bk.ru', 'Магазин')
            ->view('emails.mail')
            ->subject('Новости и акции')
            ->with([
                'text' => "Здравствуйте, $this->secondName  $this->firstName  $this->middleName, у нас для
                вас акция, подробности можно узнать по ссылке: https://google.ru"
            ]);
    }
}
