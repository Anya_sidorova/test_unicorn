<?php

namespace App\Mail;

use Illuminate\Mail\Mailable;

abstract class SendMail extends Mailable
{
    public $user;
    public string $secondName;
    public string $firstName;
    public string $middleName;

    public function __construct($user)
    {
        $this->user = $user;
        $this->secondName = $this->user->second_name;
        $this->firstName = $this->user->first_name;
        $this->middleName = $this->user->middle_name;
    }

    abstract public function build();
}
