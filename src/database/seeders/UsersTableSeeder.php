<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('users')->insert([
            'second_name' => 'Сидорова',
            'first_name' => 'Анна',
            'middle_name' => 'Вячеславовна',
            'email' => 'anka79sidorova@yandex.ru',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('users')->insert([
            'second_name' => 'test1',
            'first_name' => 'test11',
            'middle_name' => 'test111',
            'email' => 'anka79sidorova@yandex.ru',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('users')->insert([
            'second_name' => 'test2',
            'first_name' => 'test22',
            'middle_name' => 'test222',
            'email' => 'anka79sidorova@yandex.ru',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('users')->insert([
            'second_name' => 'test3',
            'first_name' => 'test33',
            'middle_name' => 'test333',
            'email' => 'anka79sidorova@yandex.ru',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('users')->insert([
            'second_name' => 'test4',
            'first_name' => 'test44',
            'middle_name' => 'test444',
            'email' => 'anka79sidorova@yandex.ru',
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }
}
