<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ActionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('actions')->insert([
            'title' => 'акция1',
            'date_start' => '2023-09-28',
            'date_end' => '2023-09-05',
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }
}
