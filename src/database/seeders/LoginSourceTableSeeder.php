<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LoginSourceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('login_sources')->insert([
            'user_id' => 1,
            'tms' => '2023-10-06 16:55:24',
            'source' => 'site'
        ]);
        DB::table('login_sources')->insert([
            'user_id' => 2,
            'tms' => '2023-10-03 16:55:34',
            'source' => 'android'
        ]);
        DB::table('login_sources')->insert([
            'user_id' => 2,
            'tms' => '2023-09-25 16:55:54',
            'source' => 'iphone'
        ]);
        DB::table('login_sources')->insert([
            'user_id' => 2,
            'tms' => '2023-09-12 16:56:32',
            'source' => 'site'
        ]);
        DB::table('login_sources')->insert([
            'user_id' => 2,
            'tms' => '2023-07-06 17:13:18',
            'source' => 'site'
        ]);
        DB::table('login_sources')->insert([
            'user_id' => 5,
            'tms' => '2023-09-15 20:03:08',
            'source' => 'site'
        ]);
        DB::table('login_sources')->insert([
            'user_id' => 5,
            'tms' => '2023-09-01 20:06:15',
            'source' => 'site'
        ]);
    }
}
